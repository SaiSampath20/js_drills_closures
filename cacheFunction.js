function cacheFunction(cb) {
    cache = {};

    if (cb == undefined) {
        return cache;
    }
    return function returnCache(...parameters) {
        if (cache[parameters] == undefined) {
            cache[parameters] = cb(...parameters);
        }
        return cache[parameters];
    }
}

module.exports = cacheFunction;

