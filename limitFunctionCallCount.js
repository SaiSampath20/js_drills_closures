function limitFunctionCallCount(cb, n) {
    let number_of_calls = 0;
    if (n == undefined) {
        n = 1;
    }
    if (cb == undefined) {
        return null;
    }
    return function call_n_times(...parameters) {
        if (number_of_calls < n) {
            number_of_calls += 1;
            const return_value = cb(...parameters);
            if (return_value != undefined) {
                console.log(return_value);
            }
        }
    };
}

module.exports = limitFunctionCallCount;
