const limitFunctionCallCount = require("../limitFunctionCallCount");

// TestCase 1
const call_n_times1 = limitFunctionCallCount((a, b) => {
    return a + b;
}, 5);

call_n_times1("name", "pro");
call_n_times1(10, 20);

// TestCase 2
const call_n_times2 = limitFunctionCallCount((a, b) => {
    return a / b;
}, 1);

call_n_times2(10, 0);
call_n_times2(10, 0);

// TestCase 3
const call_n_times3 = limitFunctionCallCount((a, b) => {
    object = {
        a: a,
        b: b
    };
    return object;
}, 1);

call_n_times3(10, 20);
call_n_times3(10, 20);

// TestCase 4
const call_n_times4 = limitFunctionCallCount();

console.log(call_n_times4);

// TestCase 5
const call_n_times5 = limitFunctionCallCount((a, b) => {
    console.log(a * b);
}, 1);

call_n_times5(10, 20);