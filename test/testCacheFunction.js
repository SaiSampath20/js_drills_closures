const cacheFunction = require("../cacheFunction");

// TestCase 1
const returnedFunction = cacheFunction((a, b) => {
    return a + b;
});
console.log(returnedFunction(10, 20));
console.log(returnedFunction(10, 20));

// TestCase 2
const returnedFunction2 = cacheFunction();
console.log(returnedFunction2);