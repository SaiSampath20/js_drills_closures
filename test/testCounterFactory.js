const counterFactory = require("../counterFactory");

// TestCase 1
const obj = counterFactory();

const inc = obj.increment();
const dec = obj.decrement();

console.log(inc(10));
console.log(inc(20));
console.log(dec(20));
console.log(dec(30));