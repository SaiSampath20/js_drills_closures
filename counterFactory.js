function counterFactory() {
    object = {};

    object = {
        increment: function () {
            return function inc(counter) {
                counter += 1;
                return counter;
            };
        },
        decrement: function () {
            return function dec(counter) {
                counter--;
                return counter;
            };
        }
    };
    return object;
}

module.exports = counterFactory;